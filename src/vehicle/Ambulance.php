<?php

/**
 * Created by PhpStorm.
 * User: Shaon
 * Date: 6/4/2016
 * Time: 6:14 PM
 */
class Ambulance
{
    private $color;
    private $speed;
    private $capacityToContainPeople;
    
    function __construct($color, $speed, $capacityToContainPeople)
    {
        $this->color = $color;
        $this->speed = $speed;
        $this->capacityToContainPeople = $capacityToContainPeople;
    }
    
    function getColorOfAmbulance() {
        return $this->color;
    }
    
    function getSpeedOfAmbulance() {
        return $this->speed;
    }
    
    function getCapacityToContainPeopleCar() {
        return $this->capacityToContainPeople;
    }
}