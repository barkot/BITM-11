<?php

include_once ('../vendor/autoload.php');

//function __autoload($className){
//
//   $fileName= $className.".php";
//
//   include_once ($fileName);
//
//}

//include_once 'people/Student.php';
//include_once 'vehicle/Car.php';

use App\people\Student;
use App\vehicle\Car;

$firstStudent = new Student('C131007', "Md Ashiqul Islam");


echo "First Student ID: " . $firstStudent->getId() . "<br>";
echo "First Student name: " . $firstStudent->getName();
echo "<hr>";

//.............


$carOfFirstStudent = new Car("Red", "200kmph", 4, "TOYOTA");
echo "First Student's car brand name: "
    . $carOfFirstStudent->getBrandName() . "<br>";
echo "First Student's car color: "
    . $carOfFirstStudent->getColorOfCar() . "<br>";
echo "First Student's car can contain: " .
    $carOfFirstStudent->getCapacityToContainPeopleCar() . " people" . "<br>";
echo "First Student's car speed: " . $carOfFirstStudent->getSpeedOfCar() . "<br>";

echo "<hr>";

//....................

// second student

$secondStudent = new Student('C131083', "jahed Bin Yousuf");

echo "First Student ID: " . $secondStudent->getId() . "<br>";
echo "First Student name: " . $secondStudent->getName();
echo "<hr>";

//.............

$carOfSecondStudent = new Car("Blue", "100kmph", 2, "BMW");
echo "First Student's car brand name: "
    . $carOfSecondStudent->getBrandName() . "<br>";
echo "First Student's car color: "
    . $carOfSecondStudent->getColorOfCar() . "<br>";
echo "First Student's car can contain: " .
    $carOfSecondStudent->getCapacityToContainPeopleCar() . " people" . "<br>";
echo "First Student's car speed: " . $carOfSecondStudent->getSpeedOfCar() . "<br>";

echo "<hr>";
